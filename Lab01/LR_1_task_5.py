import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import accuracy_score, confusion_matrix, recall_score, precision_score, f1_score, roc_curve, \
    roc_auc_score

df = pd.read_csv('data_metrics.csv')
df.head()
df['predicted_RF'] = (df.model_RF >= 0.5).astype('int')
df['predicted_LR'] = (df.model_LR >= 0.5).astype('int')
df.head()
confusion_matrix(df.actual_label.values, df.predicted_RF.values)


def Bily_Find_TP(y_true, y_pred):
    # counts the number of true positives (y_true = 1, y_pred = 1)
    return sum((y_true == 1) & (y_pred == 1))


def Bily_Find_FN(y_true, y_pred):
    # counts the number of false negatives (y_true = 1, y_pred = 0)
    return sum((y_true == 1) & (y_pred == 0))


def Bily_Find_FP(y_true, y_pred):
    # counts the number of false negatives (y_true = 0, y_pred = 1)
    return sum((y_true == 0) & (y_pred == 1))


def Bily_Find_TN(y_true, y_pred):
    # counts the number of false negatives (y_true = 0, y_pred = 0)
    return sum((y_true == 0) & (y_pred == 0))


print('TP:', Bily_Find_TP(df.actual_label.values, df.predicted_RF.values))
print('FN:', Bily_Find_FN(df.actual_label.values, df.predicted_RF.values))
print('FP:', Bily_Find_FP(df.actual_label.values, df.predicted_RF.values))
print('TN:', Bily_Find_TN(df.actual_label.values, df.predicted_RF.values))


def Bily_find_conf_matrix_values(y_true, y_pred):
    # calculate TP, FN, FP, TN
    TP = Bily_Find_TP(y_true, y_pred)
    FN = Bily_Find_FN(y_true, y_pred)
    FP = Bily_Find_FP(y_true, y_pred)
    TN = Bily_Find_TN(y_true, y_pred)
    return TP, FN, FP, TN


def Bily_confusion_matrix(y_true, y_pred):
    TP, FN, FP, TN = Bily_find_conf_matrix_values(y_true, y_pred)
    return np.array([[TN, FP], [FN, TP]])


assert np.array_equal(Bily_confusion_matrix(df.actual_label.values, df.predicted_RF.values),
                      confusion_matrix(df.actual_label.values, df.predicted_RF.values)), ('Bily_confusion_matrix() is '
                                                                                          'not correct for RF')
assert np.array_equal(Bily_confusion_matrix(df.actual_label.values, df.predicted_LR.values),
                      confusion_matrix(df.actual_label.values, df.predicted_LR.values)), ('Bily_confusion_matrix() is '
                                                                                          'not correct for LR')

accuracy_score(df.actual_label.values, df.predicted_RF.values)


def Bily_accuracy_score(y_true, y_pred):
    # calculates the fraction of samples
    TP, FN, FP, TN = Bily_find_conf_matrix_values(y_true, y_pred)
    return (TP + TN) / (TP + FP + TN + FN)


assert Bily_accuracy_score(df.actual_label.values,
                           df.predicted_RF.values) == accuracy_score(df.actual_label.values,
                                                                     df.predicted_RF.values), ('Bily_accuracy_score '
                                                                                               'failed on RF')
assert Bily_accuracy_score(df.actual_label.values,
                           df.predicted_LR.values) == accuracy_score(df.actual_label.values,
                                                                     df.predicted_LR.values), ('Bily_accuracy_score '
                                                                                               'failed on LR')

print('Accuracy RF: %.3f' % (Bily_accuracy_score(df.actual_label.values, df.predicted_RF.values)))
print('Accuracy LR: %.3f' % (Bily_accuracy_score(df.actual_label.values, df.predicted_LR.values)))

recall_score(df.actual_label.values, df.predicted_RF.values)


def Bily_recall_score(y_true, y_pred):
    # calculates the fraction of positive samples predicted correctly
    TP, FN, FP, TN = Bily_find_conf_matrix_values(y_true, y_pred)
    return TP / (TP + FN)


assert Bily_recall_score(df.actual_label.values,
                         df.predicted_RF.values) == recall_score(df.actual_label.values,
                                                                 df.predicted_RF.values), ('Bily_recall_score failed '
                                                                                           'on RF')
assert Bily_recall_score(df.actual_label.values,
                         df.predicted_LR.values) == recall_score(df.actual_label.values,
                                                                 df.predicted_LR.values), ('Bily_recall_score failed '
                                                                                           'on LR')

print('Recall RF: %.3f' % (Bily_recall_score(df.actual_label.values, df.predicted_RF.values)))
print('Recall LR: %.3f' % (Bily_recall_score(df.actual_label.values, df.predicted_LR.values)))

precision_score(df.actual_label.values, df.predicted_RF.values)


def Bily_precision_score(y_true, y_pred):
    # calculates the fraction of predicted positives samples that are actually positive
    TP, FN, FP, TN = Bily_find_conf_matrix_values(y_true, y_pred)
    return TP / (TP + FP)


assert Bily_precision_score(df.actual_label.values,
                            df.predicted_RF.values) == precision_score(df.actual_label.values,
                                                                       df.predicted_RF.values), ('Bily_precision_score '
                                                                                                 'failed on RF')
assert Bily_precision_score(df.actual_label.values,
                            df.predicted_LR.values) == precision_score(df.actual_label.values,
                                                                       df.predicted_LR.values), ('Bily_precision_score '
                                                                                                 'failed on LR')

print('Precision RF: %.3f' % (Bily_precision_score(df.actual_label.values, df.predicted_RF.values)))
print('Precision LR: %.3f' % (Bily_precision_score(df.actual_label.values, df.predicted_LR.values)))

f1_score(df.actual_label.values, df.predicted_RF.values)


def Bily_f1_score(y_true, y_pred):
    # calculates the F1 score
    recall = Bily_recall_score(y_true, y_pred)
    precision = Bily_precision_score(y_true, y_pred)
    return 2 * (recall * precision) / (recall + precision)


assert Bily_f1_score(df.actual_label.values,
                     df.predicted_RF.values) == f1_score(df.actual_label.values,
                                                         df.predicted_RF.values), 'Bily_f1_score failed on RF'
assert Bily_f1_score(df.actual_label.values,
                     df.predicted_LR.values) == f1_score(df.actual_label.values,
                                                         df.predicted_LR.values), 'Bily_f1_score failed on LR'

print('F1 RF: %.3f' % (Bily_f1_score(df.actual_label.values, df.predicted_RF.values)))
print('F1 LR: %.3f' % (Bily_f1_score(df.actual_label.values, df.predicted_LR.values)))

print('scores with threshold = 0.5')
print('Accuracy RF: %.3f' % (Bily_accuracy_score(df.actual_label.values, df.predicted_RF.values)))
print('Recall RF: %.3f' % (Bily_recall_score(df.actual_label.values, df.predicted_RF.values)))
print('Precision RF: %.3f' % (Bily_precision_score(df.actual_label.values, df.predicted_RF.values)))
print('F1 RF: %.3f' % (Bily_f1_score(df.actual_label.values, df.predicted_RF.values)))
print('')
print('scores with threshold = 0.25')
print('Accuracy RF: %.3f' % (Bily_accuracy_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
print('Recall RF: %.3f' % (Bily_recall_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
print('Precision RF: %.3f' % (Bily_precision_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))
print('F1 RF: %.3f' % (Bily_f1_score(df.actual_label.values, (df.model_RF >= 0.25).astype('int').values)))

fpr_RF, tpr_RF, thresholds_RF = roc_curve(df.actual_label.values, df.model_RF.values)
fpr_LR, tpr_LR, thresholds_LR = roc_curve(df.actual_label.values, df.model_LR.values)

auc_RF = roc_auc_score(df.actual_label.values, df.model_RF.values)
auc_LR = roc_auc_score(df.actual_label.values, df.model_LR.values)

print('AUC RF:%.3f' % auc_RF)
print('AUC LR:%.3f' % auc_LR)

plt.plot(fpr_RF, tpr_RF, 'r-', label='RF AUC: %.3f' % auc_RF)
plt.plot(fpr_LR, tpr_LR, 'b-', label='LR AUC: %.3f' % auc_LR)
plt.plot([0, 1], [0, 1], 'k-', label='random')
plt.plot([0, 0, 1, 1], [0, 1, 1, 1], 'g-', label='perfect')
plt.legend()
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.show()
