import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot
from sklearn import preprocessing
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.multiclass import OneVsOneClassifier
from sklearn.model_selection import train_test_split, cross_val_score, StratifiedKFold
from sklearn.tree import DecisionTreeClassifier

input_file = "income_data.txt"

X = []
y = []
count_class1 = 0
count_class2 = 0
max_datapoints = 25000

with open(input_file, "r") as f:
    for line in f.readlines():
        if count_class1 >= max_datapoints and count_class2 >= max_datapoints:
            break

        if "?" in line:
            continue

        data = line[:-1].split(", ")

        if data[-1] == "<=50K" and count_class1 < max_datapoints:
            X.append(data)
            count_class1 += 1

        if data[-1] == ">50K" and count_class2 < max_datapoints:
            X.append(data)
            count_class2 += 1

X = np.array(X)

label_encoder = []

X_encoded = np.empty(X.shape)

for i, item in enumerate(X[0]):
    if item.isdigit():
        X_encoded[:, i] = X[:, i]
    else:
        curr_label_encoder = preprocessing.LabelEncoder()
        label_encoder.append(curr_label_encoder)
        X_encoded[:, i] = curr_label_encoder.fit_transform(X[:, i])

X = X_encoded[:, :-1].astype(int)
y = X_encoded[:, -1].astype(int)

X_train, X_validation, Y_train, Y_validation = train_test_split(
    X, y, test_size=0.2, random_state=1
)

models = []
names = []
results = []

models.append(("LR", LogisticRegression(solver="liblinear", multi_class="ovr")))
models.append(("LDA", LinearDiscriminantAnalysis()))
models.append(("KNN", KNeighborsClassifier()))
models.append(("CART", DecisionTreeClassifier()))
models.append(("NB", GaussianNB()))
models.append(("SVM", OneVsOneClassifier(LinearSVC(random_state=0, dual=False))))

print("Accuracy:")
for name, model in models:
    kfold = StratifiedKFold(n_splits=10, random_state=1, shuffle=True)
    cv_results = cross_val_score(model, X_train, Y_train, cv=kfold, scoring="accuracy")
    names.append(name)
    results.append(cv_results)
    print("%s: %f (%f)" % (name, cv_results.mean(), cv_results.std()))

print()

pyplot.boxplot(results, labels=names)
pyplot.title("Algorithm Comparison")
pyplot.show()

model = OneVsOneClassifier(LinearSVC(random_state=0, dual=False))
model.fit(X_train, Y_train)
predictions = model.predict(X_validation)

print("Accuracy score: ", accuracy_score(Y_validation, predictions))
print("Confusion matrix: \n", confusion_matrix(Y_validation, predictions))
print("Classification report: \n", classification_report(Y_validation, predictions))

input_data = [
    "37",
    "Private",
    "215646",
    "HS-grad",
    "9",
    "Never-married",
    "Handlers-cleaners",
    "Not-in-family",
    "White",
    "Male",
    "0",
    "0",
    "40",
    "United-States",
]

input_data_encoded = [-1] * len(input_data)
count = 0
for i, item in enumerate(input_data):
    if item.isdigit():
        input_data_encoded[i] = int(input_data[i])
    else:
        encoder = label_encoder[count]
        input_data_encoded[i] = int(encoder.transform([(input_data[i])])[-1])
        count += 1

input_data_encoded = np.array([input_data_encoded])

for name, model in models:
    model.fit(X_train, Y_train)
    prediction = model.predict(input_data_encoded)
    print(name)
    print("Прогноз: {}".format(label_encoder[-1].inverse_transform(prediction)[0]))
    print("Accuracy score: ", accuracy_score(Y_validation, predictions))
    print("Confusion matrix: \n", confusion_matrix(Y_validation, predictions))
    print("Classification report: \n", classification_report(Y_validation, predictions))
