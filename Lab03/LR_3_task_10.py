import datetime
import json
import numpy as np
from sklearn import covariance, cluster
import yfinance as yf

input_file = "company_symbol_mapping.json"

with open(input_file, "r") as f:
    company_symbols_map = json.loads(f.read())

symbols, names = np.array(list(company_symbols_map.items())).T

start_date = datetime.datetime(2003, 7, 3)
end_date = datetime.datetime(2007, 5, 4)

quotes = []
valid = []
for s in symbols:
    try:
        data = yf.download(s, start=start_date, end=end_date)
        if not data.empty:
            quotes.append(data)
            valid.append(s)
    except Exception as e:
        print(f"Failed to download data for {s}: {e}")

if not quotes:
    print("No valid data available for any symbol. Check your symbol mapping and data availability.")
else:
    symbols = valid

opening_quotes = np.array([quote["Open"].values for quote in quotes]).T
closing_quotes = np.array([quote["Close"].values for quote in quotes]).T

quotes_diff = closing_quotes - opening_quotes

X = quotes_diff.copy()
X /= X.std(axis=0)

edge_model = covariance.GraphicalLassoCV()

with np.errstate(invalid="ignore"):
    edge_model.fit(X)

_, labels = cluster.affinity_propagation(edge_model.covariance_)
num_labels = labels.max()

print("\nResult:\n")
for i in range(num_labels + 1):
    print("Cluster", i + 1, "==>", ", ".join(names[np.where(labels == i)[0]]))